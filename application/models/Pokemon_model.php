<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pokemon_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getPokemons($select = "*", $where = false, $value = false, $returnType = 'array') { //fonctions qui permet de selectionner des pokemons de la BDD
	    $this->db->select($select)
            ->from('pokemons');

        if($where && $value){
            $this->db->where($where,$value);
        }

        $result = $this->db->get(); //On definis la variable result

        if($returnType === 'row') { //Si le résultat n'est qu'une seule ligne
            return $result->row();
        } else { //Sinon, si il retourne un tableau
            return $result->result(); //On prevois leventuel bug, si il y a au minimum un resultat la code se apsse comme prevue

        }
    }


    public function comparePokemon($poke1, $poke2) {

        $pokemon1HP = $poke1->pok_hp;
        $pokemon1Weight = $poke1->pok_weight;
        $pokemon1Height = $poke1->pok_height;
        $pokemon1atk = $poke1->pok_atk;
        $pokemon1def = $poke1->pok_def;

        $pokemon2HP = $poke2->pok_hp;
        $pokemon2Weight = $poke2->pok_weight;
        $pokemon2Height = $poke2->pok_height;
        $pokemon2atk = $poke2->pok_atk;
        $pokemon2def = $poke2->pok_def;

        $compare = new stdClass();

        if ($pokemon1HP > $pokemon2HP) {
            $compare->hp = $pokemon1HP . ' > ' . $pokemon2HP;
        } else if ($pokemon1HP < $pokemon2HP) {
            $compare->hp = $pokemon1HP . ' < ' . $pokemon2HP;
        } else {
            $compare->hp = $pokemon1HP . ' = ' . $pokemon2HP;
        }

        if ($pokemon1Weight > $pokemon2Weight) {
            $compare->weight = $pokemon1Weight . ' > ' . $pokemon2Weight;
        } else if ($pokemon1HP < $pokemon2Weight) {
            $compare->weight = $pokemon1Weight . ' < ' . $pokemon2Weight;
        } else {
            $compare->weight = $pokemon1Weight . ' = ' . $pokemon2Weight;
        }

        if ($pokemon1Height > $pokemon2Height) {
            $compare->height = $pokemon1Height . ' > ' . $pokemon2Height;
        } else if ($pokemon1Height < $pokemon2Height) {
            $compare->height = $pokemon1Height . ' < ' . $pokemon2Height;
        } else {
            $compare->height = $pokemon1Height . ' = ' . $pokemon2Height;
        }

        if ($pokemon1atk > $pokemon2atk) {
            $compare->attaque = $pokemon1atk . ' > ' . $pokemon2atk;
        } else if ($pokemon1atk < $pokemon2atk) {
            $compare->attaque = $pokemon1atk . ' < ' . $pokemon2atk;
        } else {
            $compare->attaque = $pokemon1atk . ' = ' . $pokemon2atk;
        }

        if ($pokemon1def > $pokemon2def) {
            $compare->defense = $pokemon1def . ' > ' . $pokemon2def;
        } else if ($pokemon1def < $pokemon2def) {
            $compare->defense = $pokemon1def . ' < ' . $pokemon2def;
        } else {
            $compare->defense = $pokemon1def . ' = ' . $pokemon2def;
        }


        // die(var_dump($compare));

        return $compare;

    }

    public function checkExistPokemons($where) {

        // die(var_dump($where));

        $query = $this->db->select('*')
            ->from('pokemons')
            ->where($where)
            ->get();
        //die(var_dump($this->db->last_query()));
        if ($query->num_rows() > 0) {

            return $query->row();
        } else {
            return false;
        }

    }

    public function getPokPref($id_user)
    {

        return  $this->db->select("p.pok_name, p.pok_img_url")
            ->from('pokemons p')
            ->join('users u','p.id = u.pok_pref')
            ->where('u.id', $id_user)
            ->get()->row();



    }

    public function getTypes($select, $where = null, $value = null, $returnType = 'array') {

        //die(var_dump($returnType));

        $this->db->select($select)
            ->from('poke_type');

        if ($where != null && $value != null) {
            $this->db->where($where, $value);
        }

        $result = $this->db->get();

        if ($returnType === 'row') {
            if ($result->num_rows() > 0) {
                return $result->row();
            } else {
                return false;
            }
        } else {
            if ($result->num_rows() > 0) {
                return $result->result();
            } else {
                return false;
            }
        }
    }

    public function checkExistPokemon($where,$value) {


        $query = $this->db->select('*')
            ->from('pokemons')
            ->where($where,$value)
            ->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }

    }

    public function checkExistTypes($where,$value) {


        $query = $this->db->select('*')
            ->from('poke_type')
            ->where($where,$value)
            ->get();

        return $query->row();

    }

    public function getPokemonsLike($select, $like, $returnType = 'array') {

        $this->db->select($select)
            ->from('pokemons')
            ->like('pok_name', $like, 'after');

        $result = $this->db->get();

        if ($returnType === 'row') {
            return $result->row();
        } else {
            return $result->result();
        }
    }
}
